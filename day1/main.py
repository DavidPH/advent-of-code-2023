import re


def part1():
    with open("input.txt", "r") as file:
        sum = 0
        for line in file:
            line = line.strip()
            digits = re.findall(r"\d", line)
            if not digits:
                continue

            number = int(digits[0] + digits[-1])
            sum += number
        print(sum)


def part2():
    numbers = {
        "one": "o1e",
        "two": "t2p",
        "three": "t3e",
        "four": "f4r",
        "five": "f5e",
        "six": "s6x",
        "seven": "s7n",
        "eight": "e8t",
        "nine": "n9e"
    }

    with open("input.txt", "r") as file:
        sum = 0
        for line in file:
            line = line.strip()

            for spelled_number, digit in numbers.items():
                line = line.replace(spelled_number, digit)

            digits = re.findall(r"\d", line)
            if not digits:
                continue

            number = int(digits[0] + digits[-1])
            sum += number
        print(sum)


part1()
part2()
