from functools import cache


def load(file: str):
    with open(file, "r") as f:
        return f.read()


def shiftRocks(rocks: list[list[str]]):
    height = len(rocks)
    width = len(rocks[0])
    for i in range(height):
        for j in range(width):
            if rocks[i][j] == "O":
                new_i = i
                rocks[i][j] = "."
                while new_i > -1 and rocks[new_i][j] == ".":
                    new_i -= 1
                new_i += 1
                rocks[new_i][j] = "O"


@cache
def cycleRocks(problem: str):
    rocks = list(map(list, problem.splitlines()))
    for _ in range(4):
        shiftRocks(rocks)
        rocks = list(map(list, zip(*rocks[::-1])))
    return "\n".join(map(lambda l: "".join(l), rocks))


def part1(problem: str):
    rocks = list(map(list, problem.splitlines()))
    shiftRocks(rocks)
    total = 0
    for i, line in enumerate(rocks):
        for char in line:
            if char == "O":
                total += len(rocks) - i
    print(total)


def part2(problem: str):
    cycles = {}
    p = 0
    cycle_count = 1_000_000_000
    while p < cycle_count:
        problem = cycleRocks(problem)
        p += 1
        if problem in cycles:
            period = p - cycles[problem]
            p += ((cycle_count-p)//period)*period

        cycles[problem] = p

    total = 0
    problem = problem.splitlines()
    for i, line in enumerate(problem):
        for char in line:
            if char == "O":
                total += len(problem) - i
    print(total)


problem = load("day14/input.txt")
part1(problem)
part2(problem)
