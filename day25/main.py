from collections import defaultdict


def load(file: str):
    with open(file, "r") as f:
        return [line.strip() for line in f]


problem = load("day25/input.txt")

graph: dict[str, list[str]] = defaultdict(list)
edges: list[tuple[str, str]] = []

for line in problem:
    origin, *rest = line.replace(":", "").split(" ")
    for con in rest:
        pair = tuple(sorted([origin, con]))
        if pair not in edges:
            edges.append(pair)

remove = [("fht", "vtt"), ("czs", "tdk"), ("bbg", "kbr")]
remove = list(map(lambda x: tuple(sorted(x)), remove))

o = "digraph G {\n" # https://dreampuf.github.io/GraphvizOnline
for (u, v) in edges:
    o += f'"{u}" -> "{v}";\n'
    if (u, v) in remove:
        continue
    graph[u].append(v)
    graph[v].append(u)
o += "}"

with open("day25/output.txt", "w") as f:
    f.write(o)


def dfs(graph, start):
    visited = set()
    stack = [start]

    while stack:
        node = stack.pop()
        if node not in visited:
            visited.add(node)
            stack.extend(
                neighbor for neighbor in graph[node] if neighbor not in visited)

    return visited


visited = set()
total = 1
for (u, v) in edges:
    if u not in visited:
        x = dfs(graph, u)
        visited = visited.union(x)
        total *= len(x)
print(total)
