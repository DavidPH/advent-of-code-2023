import re


def part1():
    with open("day2/input.txt", "r") as file:

        colors = {
            "red": 12,
            "green": 13,
            "blue": 14
        }

        pattern = re.compile(r'(\d+)\s+(\w+)')

        sum = 0
        for line in file:
            line = line.strip()
            game, rounds = line.split(":")
            rounds = rounds.split(";")
            id = int(game.split(" ")[1])

            possible = True
            for round in rounds:
                for count, color in pattern.findall(round):
                    if int(count) > colors[color]:
                        possible = False
                        break
            if possible:
                sum += id
        print(sum)


def part2():
    with open("day2/input.txt", "r") as file:
        pattern = re.compile(r'(\d+)\s+(\w+)')
        sum = 0
        for line in file:
            colors = {
                "red": 0,
                "green": 0,
                "blue": 0
            }
            line = line.strip()
            game, rounds = line.split(":")
            rounds = rounds.split(";")
            id = int(game.split(" ")[1])

            for round in rounds:
                for count, color in pattern.findall(round):
                    if int(count) > colors[color]:
                        colors[color] = int(count)
            power = 1
            for count in colors.values():
                power *= count
            sum += power
        print(sum)


part1()
part2()
