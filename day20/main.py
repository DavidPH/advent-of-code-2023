from math import lcm


def load(file: str):
    with open(file, "r") as f:
        return [line.strip() for line in f]


problem = load("day20/input.txt")


class Module:
    def __init__(self, line: str) -> None:
        name, connections = line.split("->")
        self.connections = [c.strip() for c in connections.split(",")]
        self.type = None
        if name[0] == "%" or name[0] == "&":
            self.type = name[0]
            name = name[1:]
        self.name = name.strip()

        self.count = {0: 0, 1: 0}

        self.flipFlop = False
        self.pulses = {}

    def sendPulse(self, pulse: int, sender: str) -> list[tuple[str, str, int]]:
        self.count[pulse] += 1

        if self.type == None and self.name == "broadcaster":
            return [(self.name, name, pulse) for name in self.connections]

        if self.type == "%":
            if pulse == 1:
                return []
            self.flipFlop = not self.flipFlop
            pulse = 1 if self.flipFlop else 0
            return [(self.name, name, pulse) for name in self.connections]

        if self.type == "&":
            self.pulses[sender] = pulse
            for val in self.pulses.values():
                if val == 0:
                    return [(self.name, name, 1) for name in self.connections]
            return [(self.name, name, 0) for name in self.connections]


modules: dict[str, 'Module'] = {}

conjuctions: list['Module'] = []
for line in problem:
    m = Module(line)
    modules[m.name] = m
    if m.type == "&":
        conjuctions.append(m)

for conjuction in conjuctions:
    for name, module in modules.items():
        if conjuction.name in module.connections:
            conjuction.pulses[module.name] = 0


pulse_count = {0: 0, 1: 0}
button_presses = 5000
lcms = {}
for i in range(button_presses):
    pulses = modules["broadcaster"].sendPulse(0, "button")
    pulse_count[0] += 1
    button_press = i + 1
    while pulses:
        new_pulses = []
        for sender, receiver, pulse in pulses:
            pulse_count[pulse] += 1
            # https://dreampuf.github.io/GraphvizOnline/#digraph%20G%20%7B%0D%0A%20%20%20%20%20node%20%5Bfillcolor%3D%22cyan%22%20style%3Dfilled%5D%20%22jq%22%3B%20%0D%0A%20node%20%5Bfillcolor%3D%22cyan%22%20style%3Dfilled%5D%20%22nx%22%3B%20%0D%0A%20node%20%5Bfillcolor%3D%22cyan%22%20style%3Dfilled%5D%20%22sp%22%3B%20%0D%0A%20%20%20%20node%20%5Bfillcolor%3D%22cyan%22%20style%3Dfilled%5D%20%22cc%22%3B%20%0D%0A%09node%20%5Bfillcolor%3D%22cyan%22%20style%3Dfilled%5D%20%22dd%22%3B%20%20%20%0D%0A%09node%20%5Bfillcolor%3D%22white%22%20style%3Dfilled%5D%20%22rx%22%3B%20%0D%0A%0D%0Anr%20-%3E%20hq%3B%0D%0Axk%20-%3E%20sn%3B%0D%0Acl%20-%3E%20xk%3B%0D%0Amj%20-%3E%20dq%3B%0D%0Amj%20-%3E%20qr%3B%0D%0Agm%20-%3E%20lk%3B%0D%0Agm%20-%3E%20cl%3B%0D%0Amv%20-%3E%20lz%3B%0D%0Amv%20-%3E%20rr%3B%0D%0Aqr%20-%3E%20cz%3B%0D%0Aqr%20-%3E%20sp%3B%0D%0Aqr%20-%3E%20lb%3B%0D%0Aqr%20-%3E%20xt%3B%0D%0Aqr%20-%3E%20fx%3B%0D%0Axt%20-%3E%20vl%3B%0D%0Add%20-%3E%20rx%3B%0D%0Arv%20-%3E%20qr%3B%0D%0Ats%20-%3E%20lz%3B%0D%0Ats%20-%3E%20nk%3B%0D%0Avl%20-%3E%20zj%3B%0D%0Avl%20-%3E%20qr%3B%0D%0Aqm%20-%3E%20db%3B%0D%0Aqm%20-%3E%20lk%3B%0D%0Asn%20-%3E%20lp%3B%0D%0Axc%20-%3E%20lz%3B%0D%0Ajn%20-%3E%20sz%3B%0D%0Ajn%20-%3E%20ft%3B%0D%0Avg%20-%3E%20lk%3B%0D%0Avg%20-%3E%20ks%3B%0D%0Ahq%20-%3E%20ft%3B%0D%0Ahq%20-%3E%20lh%3B%0D%0Alz%20-%3E%20gx%3B%0D%0Alz%20-%3E%20xn%3B%0D%0Alz%20-%3E%20jq%3B%0D%0Alz%20-%3E%20fb%3B%0D%0Alz%20-%3E%20ts%3B%0D%0Alz%20-%3E%20rr%3B%0D%0Ank%20-%3E%20mv%3B%0D%0Ank%20-%3E%20lz%3B%0D%0Anx%20-%3E%20dd%3B%0D%0Asp%20-%3E%20dd%3B%0D%0Ajj%20-%3E%20qr%3B%0D%0Ajj%20-%3E%20mj%3B%0D%0Asz%20-%3E%20nr%3B%0D%0Asz%20-%3E%20ft%3B%0D%0Arn%20-%3E%20qm%3B%0D%0Acz%20-%3E%20xt%3B%0D%0Acz%20-%3E%20qr%3B%0D%0Afr%20-%3E%20ft%3B%0D%0Avb%20-%3E%20lz%3B%0D%0Avb%20-%3E%20xn%3B%0D%0Abroadcaster%20-%3E%20cz%3B%0D%0Abroadcaster%20-%3E%20gm%3B%0D%0Abroadcaster%20-%3E%20jn%3B%0D%0Abroadcaster%20-%3E%20ts%3B%0D%0Afb%20-%3E%20vb%3B%0D%0Ahd%20-%3E%20lz%3B%0D%0Ahd%20-%3E%20xc%3B%0D%0Agx%20-%3E%20fb%3B%0D%0Adb%20-%3E%20mh%3B%0D%0Adb%20-%3E%20lk%3B%0D%0Aft%20-%3E%20jx%3B%0D%0Aft%20-%3E%20nx%3B%0D%0Aft%20-%3E%20lh%3B%0D%0Aft%20-%3E%20pc%3B%0D%0Aft%20-%3E%20nr%3B%0D%0Aft%20-%3E%20jn%3B%0D%0Aft%20-%3E%20kr%3B%0D%0Aqc%20-%3E%20pl%3B%0D%0Aqc%20-%3E%20ft%3B%0D%0Afx%20-%3E%20bz%3B%0D%0Ajx%20-%3E%20kr%3B%0D%0Apl%20-%3E%20ft%3B%0D%0Apl%20-%3E%20fr%3B%0D%0Alh%20-%3E%20jx%3B%0D%0Arr%20-%3E%20gx%3B%0D%0Acc%20-%3E%20dd%3B%0D%0Axn%20-%3E%20xl%3B%0D%0Akr%20-%3E%20pc%3B%0D%0Axl%20-%3E%20dv%3B%0D%0Axl%20-%3E%20lz%3B%0D%0Adq%20-%3E%20qr%3B%0D%0Adq%20-%3E%20rv%3B%0D%0Amh%20-%3E%20lk%3B%0D%0Amh%20-%3E%20vg%3B%0D%0Asb%20-%3E%20lk%3B%0D%0Asb%20-%3E%20rn%3B%0D%0Abz%20-%3E%20lb%3B%0D%0Abz%20-%3E%20qr%3B%0D%0Aks%20-%3E%20lk%3B%0D%0Aqh%20-%3E%20ft%3B%0D%0Aqh%20-%3E%20qc%3B%0D%0Apc%20-%3E%20qh%3B%0D%0Alb%20-%3E%20mb%3B%0D%0Adv%20-%3E%20lz%3B%0D%0Adv%20-%3E%20hd%3B%0D%0Amb%20-%3E%20qr%3B%0D%0Amb%20-%3E%20jj%3B%0D%0Azj%20-%3E%20fx%3B%0D%0Azj%20-%3E%20qr%3B%0D%0Alp%20-%3E%20sb%3B%0D%0Ajq%20-%3E%20dd%3B%0D%0Alk%20-%3E%20sn%3B%0D%0Alk%20-%3E%20cc%3B%0D%0Alk%20-%3E%20xk%3B%0D%0Alk%20-%3E%20rn%3B%0D%0Alk%20-%3E%20gm%3B%0D%0Alk%20-%3E%20cl%3B%0D%0Alk%20-%3E%20lp%3B%0D%0A%0D%0A%20%20%20%20%0D%0A%7D%0D%0A
            if receiver == "dd" and pulse == 1 and sender not in lcms:
                lcms[sender] = button_press
            if not receiver in modules:
                continue
            new_pulses += modules[receiver].sendPulse(pulse, sender)
        pulses = new_pulses
    if button_press == 1000:
        print(pulse_count[0] * pulse_count[1])


print(lcm(*[val for val in lcms.values()]))
