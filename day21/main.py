import numpy as np


def load(file: str):
    with open(file, "r") as f:
        return [line.strip() for line in f]


P = complex


class Grid:
    def __init__(self, input: list[str]) -> None:
        self.height = len(input)
        self.width = len(input[0])
        self.tiles: dict['P', str] = {}
        for i, line in enumerate(input):
            for j, char in enumerate(line):
                point = P(i, j)
                if char == "#":
                    self.tiles[point] = char
                if char == "S":
                    self.start = point

    def takeStep(self, positions: set['P']):
        directions = [
            P(-1, 0), P(1, 0), P(0, 1), P(0, -1)
        ]
        new_positions = set()
        for point in positions:
            for dir in directions:
                new_dir = point + dir
                if self.P(new_dir) not in self.tiles:
                    new_positions.add(new_dir)
        return new_positions

    def takeSteps(self, count: int):

        positions = {self.start}
        for _ in range(count):
            positions = self.takeStep(positions)

        return len(positions)

    def P(self, point: 'P'):
        return P(point.real % self.height, point.imag % self.width)


def part1(problem):
    grid = Grid(problem)
    print(grid.takeSteps(64))


def part2(problem):
    grid = Grid(problem)
    pos = {grid.start}
    lst = []
    for s in range(65 + 131*2 + 1):
        if s % 131 == 65:
            lst.append(len(pos))
        pos = grid.takeStep(pos)

    # https://www.wolframalpha.com/input?i=quadratic+fit+calculator&assumption=%7B%22F%22%2C+%22QuadraticFitCalculator%22%2C+%22data3x%22%7D+-%3E%22%7B0%2C+1%2C+2%7D%22&assumption=%7B%22F%22%2C+%22QuadraticFitCalculator%22%2C+%22data3y%22%7D+-%3E%22%7B3787%2C+33976%2C+94315%7D%22
    target = (26501365 - 65) // 131
    poly = np.rint(np.polynomial.polynomial.polyfit([0, 1, 2], lst, 2))
    print(int(sum(poly[i] * target**i for i in range(3))))


problem = load("day21/input.txt")
part1(problem)
part2(problem)
