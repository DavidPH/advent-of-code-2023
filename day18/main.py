def load(file: str):
    with open(file, "r") as f:
        return [line.strip().split(" ") for line in f]


directions: dict[str, tuple[int, int]] = {
    "R": (1, 0),
    "D": (0, 1),
    "L": (-1, 0),
    "U": (0, -1),
}


def part1(problem):
    current_pos = [0, 0]
    perimeter = 0
    points = [tuple(current_pos)]

    for dir, num, _ in problem:
        num = int(num)
        dir_x, dir_y = directions[dir]
        perimeter += num
        current_pos[0] += dir_x * num
        current_pos[1] += dir_y * num
        points.append(tuple(current_pos))

    area = 0
    for (x1, y1), (x2, y2) in zip(points[:-1], points[1:]):
        area += (x2-x1) * (y2+y1)

    print((abs(area) + perimeter) // 2 + 1)


def part2(problem):
    current_pos = [0, 0]
    perimeter = 0
    points = [tuple(current_pos)]

    for _, _, color in problem:
        steps = int(color[2:-2], 16)
        dir = list(directions.values())[int(color[-2:-1], 16)]

        dir_x, dir_y = dir
        perimeter += steps
        current_pos[0] += dir_x * steps
        current_pos[1] += dir_y * steps
        points.append(tuple(current_pos))

    area = 0
    for (x1, y1), (x2, y2) in zip(points[:-1], points[1:]):
        area += (x2-x1) * (y2+y1)

    print((abs(area) + perimeter) // 2 + 1)


problem = load("day18/input.txt")

part1(problem)
part2(problem)
