from functools import cache


def load(file: str):
    with open(file, "r") as f:
        return [line.strip().split() for line in f]


@cache
def SolveLine(index, group_index, springs: str, groups: tuple[int]):
    if group_index >= len(groups):
        if index < len(springs) and "#" in springs[index:]:
            return 0
        return 1

    if index >= len(springs):
        return 0

    count = 0
    group = groups[group_index]
    new_index = index + group
    if springs[index] == '?':
        if '.' not in springs[index:new_index] and springs[new_index] != '#':
            count = SolveLine(new_index + 1, group_index + 1, springs, groups) + \
                SolveLine(index + 1, group_index, springs, groups)
        else:
            count = SolveLine(index + 1, group_index, springs, groups)
    elif springs[index] == '#':
        if '.' not in springs[index:new_index] and springs[new_index] != '#':
            count = SolveLine(new_index + 1, group_index + 1, springs, groups)
        else:
            count = 0
    elif springs[index] == '.':
        count = SolveLine(index+1, group_index, springs, groups)

    return count


def part1(problem):
    total = 0
    for line, counts in problem:
        counts = tuple(map(int, counts.split(",")))
        total += SolveLine(0, 0, line + ".", counts)
    print(total)


def part2(problem):
    total = 0
    repeat = 5
    for line, counts in problem:
        counts = tuple(map(int, counts.split(",")))
        line = "?".join([line]*repeat)
        total += SolveLine(0, 0, line + ".", counts*repeat)
    print(total)


problem = load("day12/input.txt")
part1(problem)
part2(problem)