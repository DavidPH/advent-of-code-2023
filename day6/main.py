import re
from math import sqrt


def load(file: str):
    with open(file, "r") as f:
        return [re.findall("\d+", line.strip()) for line in f]


problem = load("day6/input.txt")
times, records = problem


# t = r - b
# d = t * b
# d = (r -b) * b = b^2 -rb
# b^2 - rb - d = 0

def part1():
    total = 1
    for race_time, record in zip(times, records):
        race_time, record = int(race_time), int(record)
        b1 = int((race_time + sqrt((race_time**2) - 4 * record)) / 2)
        b2 = int((race_time - sqrt((race_time**2) - 4 * record)) / 2)
        total *= (b1 - b2)
    print(total)


def part2():
    time = int("".join(times))
    record = int("".join(records))
    b1 = int((time + sqrt((time**2) - 4 * record)) / 2)
    b2 = int((time - sqrt((time**2) - 4 * record)) / 2)
    print(b1 - b2)


part1()
part2()