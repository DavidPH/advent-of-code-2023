def load(file: str):
    with open(file, "r") as f:
        return [line.strip() for line in f]


def expand_galaxies(problem: list[str], expansion_rate=2):
    missing_vert = [0 if "#" in row else 1 for row in zip(*problem)]
    expansion_rate -= 1
    y_offset = 0
    galaxies = []
    for i, line in enumerate(problem):
        if "#" not in line:
            y_offset += expansion_rate
        for j, char in enumerate(line):
            x_offset = sum(missing_vert[:j])*expansion_rate
            if char == "#":
                galaxies.append((i+y_offset, j+x_offset))
    return galaxies


def distance(a: tuple[int, int], b: tuple[int, int]):
    x1, y1 = a
    x2, y2 = b
    return abs((x2 - x1)) + abs((y2 - y1))


def part1(problem):
    sum = 0
    galaxies = expand_galaxies(problem)
    for i, galaxy in enumerate(galaxies):
        for other in galaxies[i+1:]:
            sum += distance(galaxy, other)
    print(sum)


def part2(problem):
    sum = 0
    galaxies = expand_galaxies(problem, 1_000_000)
    for i, galaxy in enumerate(galaxies):
        for other in galaxies[i+1:]:
            sum += distance(galaxy, other)
    print(sum)


problem = load("day11/input.txt")
part1(problem)
part2(problem)
