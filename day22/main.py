import re
from collections import deque


def load(file: str):
    with open(file, "r") as f:
        return [list(map(int, re.findall("\d+", line))) for line in f]


def isIntersecting(a: list[int], b: list[int]):
    return max(a[0], b[0]) <= min(a[3], b[3]) and max(a[1], b[1]) <= min(a[4], b[4])


problem = load("day22/input.txt")

problem.sort(key=lambda e: e[2])


for i, brick in enumerate(problem):
    max_z = 1
    for sub in problem[:i]:
        if isIntersecting(brick, sub):
            max_z = max(max_z, sub[5] + 1)
    brick[5] -= brick[2] - max_z
    brick[2] = max_z


problem.sort(key=lambda e: e[2])

supports = {i: set() for i in range(len(problem))}
supported = {i: set() for i in range(len(problem))}

for i, upper in enumerate(problem):
    for j, lower in enumerate(problem[:i]):
        if isIntersecting(lower, upper) and upper[2] == lower[5] + 1:
            supports[j].add(i)
            supported[i].add(j)


def part1():
    total = 0
    for i in range(len(problem)):
        if all(len(supported[j]) >= 2 for j in supports[i]):
            total += 1
    print(total)


def part2():
    total = 0
    for i in range(len(problem)):
        q = deque(j for j in supports[i] if len(supported[j]) == 1)

        falling = set(q)
        falling.add(i)

        while q:
            j = q.popleft()
            for k in supports[j] - falling:
                if supported[k] <= falling:
                    q.append(k)
                    falling.add(k)
        total += len(falling) - 1
    print(total)


part1()
part2()
