import re


def load(file: str):
    with open(file, "r") as f:
        return [line.strip() for line in f]


def part1(problem):
    total = 0
    for card in problem:
        card = card.split(": ")[1].strip().split(" | ")
        winning_numbers = re.findall("\d+", card[0])
        have_numbers = re.findall("\d+", card[1])
        winner_count = sum(
            [1 for num in winning_numbers if num in have_numbers])
        if winner_count > 0:
            total += 2**(winner_count-1)
    print(total)


def part2(problem):
    owned_cards = [1 for _ in problem]
    for i, card in enumerate(problem):
        card = card.split(": ")[1].strip().split(" | ")
        winning_numbers = re.findall("\d+", card[0])
        have_numbers = re.findall("\d+", card[1])
        winner_count = sum(
            [1 for num in winning_numbers if num in have_numbers])
        copies = owned_cards[i]
        if winner_count > 0:
            for j in range(1, winner_count + 1):
                new_index = j + i
                if new_index < len(owned_cards):
                    owned_cards[new_index] += copies
    print(sum(owned_cards))


problem = load("day4/input.txt")
part1(problem)
part2(problem)
