import re


def load(file: str):
    with open(file, "r") as f:
        return [list(map(int, re.findall("-?\d+", line))) for line in f]


problem = load("day9/input.txt")


def predict(line: list[int]) -> int:
    new_line = []
    all_zero = True
    for i, num in enumerate(line[1:]):
        diff = num - line[i]
        if diff != 0:
            all_zero = False
        new_line.append(diff)

    return line[-1] if all_zero else line[-1] + predict(new_line)


def part1():
    total = 0
    for line in problem:
        total += predict(line)
    print(total)


def part2():
    total = 0
    for line in problem:
        total += predict(list(reversed(line)))
    print(total)


part1()
part2()
