import heapq
P = complex


def load(file: str):
    with open(file, "r") as f:
        return [line.strip() for line in f]


problem = load("day17/input.txt")


class Node:
    def __init__(self, loss: int, loc: 'P', dir: 'P', steps: int):
        self.loss = loss
        self.steps = steps
        self.tuple = (loss, loc, dir, steps)
        self.key = (loc, dir, steps)

    def __lt__(self, other: 'Node'):
        return (self.loss, self.steps) < (other.loss, other.steps)


grid = {}

height = len(problem)
width = len(problem[0])

for i, line in enumerate(problem):
    for j, char in enumerate(line):
        point = P(j, i)
        grid[point] = int(char)

end_loc = P(height - 1, width - 1)


def part1():
    Q = [Node(0, 0, 1, 0), Node(0, 0, 1j, 0)]
    visited = set()

    while Q:
        current_node = heapq.heappop(Q)

        if current_node.key in visited:
            continue

        visited.add(current_node.key)

        loss, loc, dir, steps = current_node.tuple
        if loc == end_loc:
            print(loss)
            break

        new_point = loc + dir
        if steps < 3 and new_point in grid:
            heapq.heappush(
                Q, Node(loss + grid[new_point], new_point, dir, steps+1))

        dir *= 1j  # Turn 90°
        new_point = loc + dir
        if new_point in grid:
            heapq.heappush(Q, Node(loss + grid[new_point], new_point, dir, 1))

        dir *= -1  # Turn 90°
        new_point = loc + dir
        if new_point in grid:
            heapq.heappush(Q, Node(loss + grid[new_point], new_point, dir, 1))


def part2():
    Q = [Node(0, P(0, 0), P(0, 1), 0)]
    visited = set()

    while Q:
        current_node = heapq.heappop(Q)

        if current_node.key in visited:
            continue

        visited.add(current_node.key)

        loss, loc, dir, steps = current_node.tuple
        if loc == end_loc and steps >= 4:
            print(loss)
            break

        new_point = loc + dir
        if steps < 10 and new_point in grid:
            heapq.heappush(
                Q, Node(loss + grid[new_point], new_point, dir, steps+1))

        if steps < 4:
            continue

        dir *= 1j  # Turn 90°
        new_point = loc + dir

        if new_point in grid:
            heapq.heappush(Q, Node(loss + grid[new_point], new_point, dir, 1))

        dir *= -1  # Turn 90°
        new_point = loc + dir
        if new_point in grid:
            heapq.heappush(Q, Node(loss + grid[new_point], new_point, dir, 1))


part1()
part2()
