
def load(file: str):
    with open(file, "r") as f:
        return list(map(lambda l: l.splitlines(), f.read().split("\n\n")))


problem = load("day13/input.txt")


def find_diff(a, b):
    return sum(1 for x, y in zip(a, b) if x != y)


def get_symmetry(pattern: list[str], tolerance=0):
    length = len(pattern[0])
    size = length // 2
    for i in reversed(range(1, size + 1)):
        for start in [0, length - 2 * i]:
            diff = tolerance
            for line in pattern:
                diff -= find_diff(line[start: start + i],
                                  line[start + i: start + 2 * i][::-1])
            if diff != 0:
                continue
            return start + i

    return 0


def part1():
    total = 0
    for pattern in problem:
        transposed = [row for row in zip(*pattern)]
        total += get_symmetry(pattern)
        total += get_symmetry(transposed)*100
    print(total)


def part2():
    total = 0
    for pattern in problem:
        transposed = [row for row in zip(*pattern)]
        total += get_symmetry(pattern, 1)
        total += get_symmetry(transposed, 1)*100
    print(total)


part1()
part2()
