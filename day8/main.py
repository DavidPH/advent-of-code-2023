import re
from math import lcm
from functools import reduce


def load(file: str):
    with open(file, "r") as f:
        return f.read().split("\n\n")


problem = load("day8/input.txt")

instructions, directions = problem
instructions = list(map(int, instructions.replace("L", "0").replace("R", "1")))
directions = directions.split("\n")

network: dict[str, tuple] = {}
for dir in directions:
    loc, left, right = re.findall(r"\w{3}", dir)
    network[loc] = (left, right)


def part1():
    steps = instructions.copy()
    goal = "ZZZ"
    current = "AAA"
    count = 0
    while current != goal:
        step = steps[0]
        steps = steps[1:] + [step]
        current = network[current][step]
        count += 1
    print(count)


def part2():
    nodes = [k for k in network if k.endswith("A")]
    cycles = []
    for node in nodes:
        count = 0
        cycle = []
        steps = instructions.copy()
        first_z = None
        while True:
            while count == 0 or not node.endswith("Z"):
                count += 1
                node = network[node][steps[0]]
                steps = steps[1:] + [steps[0]]

            cycle.append(count)
            if first_z is None:
                first_z = node
            elif node == first_z:
                break
        cycles.append(cycle)

    print(reduce(lcm, (c[0] for c in cycles)))


part1()
part2()
