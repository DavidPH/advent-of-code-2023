import re


def load(file: str):
    with open(file, "r") as f:
        return f.read().strip().split(",")


def hash(word: str, value=0) -> int:
    if not word:
        return value
    char = word[0]
    value += ord(char)
    value *= 17
    value = value % 256

    return hash(word[1:], value)


def part1(problem):
    total = 0
    for val in problem:
        total += hash(val)
    print(total)


def part2(problem: list[str]):
    boxes: dict[int, dict[str, int]] = {}
    for operation in problem:
        instruction = "".join(re.findall("[a-zA-Z]", operation))
        box_number = hash(instruction)
        number = re.findall("\d+", operation)

        if not box_number in boxes:
            boxes[box_number] = {}
        if number:
            number = int(number[0])
            boxes[box_number][instruction] = number
        else:
            if instruction in boxes[box_number]:
                boxes[box_number].pop(instruction)

    total = 0
    for box_number, box in boxes.items():
        for i, value in enumerate(box.values()):
            total += (box_number + 1) * (i+1) * value
    print(total)


problem = load("day15/input.txt")

part1(problem)
part2(problem)
