def load(file: str):
    with open(file, "r") as f:
        return [line.strip() for line in f]


P = complex


problem = load("day23/input.txt")


grid: dict['P', str] = {}

for i, line in enumerate(problem):
    for j, char in enumerate(line):
        grid[P(i, j)] = char
        if i == 0 and char == ".":
            start = P(i, j)
        if i == len(problem) - 1 and char == ".":
            end = P(i, j)

points = [start, end]

for point, char in grid.items():
    if char == "#":
        continue
    neighbors = 0
    for dir in (P(0, -1), P(0, 1), P(-1, 0), P(1, 0)):
        new_dir = dir + point
        if new_dir in grid and grid[new_dir] != "#":
            neighbors += 1
    if neighbors >= 3:
        points.append(point)

slopes = {
    ">": [P(0, 1)],
    "<": [P(0, -1)],
    "^": [P(-1, 0)],
    "v": [P(1, 0)],
    ".": [P(0, 1), P(0, -1), P(-1, 0), P(1, 0), P(0, 1)]
}

seen = set()


def dfs(pt: 'P', graph):
    if pt == end:
        return 0

    m = -float("inf")
    seen.add(pt)
    for nx in graph[pt]:
        if nx not in seen:
            m = max(m, dfs(nx, graph) + graph[pt][nx])
    seen.remove(pt)
    return m


def part1():
    graph = {pt: {} for pt in points}
    for pt in points:
        stack = [(0, pt)]
        seen = {pt}
        while stack:
            n, point = stack.pop()
            if n != 0 and point in points:
                graph[pt][point] = n
                continue

            for dir in slopes[grid[point]]:
                new_dir = point + dir
                if new_dir in grid and grid[new_dir] != "#" and new_dir not in seen:
                    stack.append((n+1, new_dir))
                    seen.add(new_dir)

    print(dfs(start, graph))


def part2():
    graph = {pt: {} for pt in points}
    for pt in points:
        stack = [(0, pt)]
        seen = {pt}
        while stack:
            n, point = stack.pop()
            if n != 0 and point in points:
                graph[pt][point] = n
                continue

            for dir in slopes["."]:
                new_dir = point + dir
                if new_dir in grid and grid[new_dir] != "#" and new_dir not in seen:
                    stack.append((n+1, new_dir))
                    seen.add(new_dir)

    print(dfs(start, graph))


part1()

part2()
