import re


def load(file: str):
    with open(file, "r") as f:
        return [line.strip() for line in f]


def isDigit(x):
    x = str(x)
    return len(x) == 1 and len(re.findall(r"\d", x)) == 1


def within_bounds(array: list[list[any]], i, j):
    if 0 <= i < len(array) and 0 <= j < len(array[0]):
        return True
    else:
        return False


def findAdjacents(arr: list[list], i: int, j: int, func):
    directions = [
        (-1, -1), (-1, 0), (-1, 1),
        (0, -1),           (0, 1),
        (1, -1),  (1, 0),  (1, 1)
    ]

    adj = []
    for dir_i, dir_j in directions:
        if (within_bounds(arr, i + dir_i, j+dir_j)):
            char = arr[i + dir_i][j+dir_j]
            if func(char):
                adj.append(((i + dir_i, j+dir_j), char))
    return adj


def checkAdjacent(arr: list[list], i: int, j: int, func):
    directions = [
        (-1, -1), (-1, 0), (-1, 1),
        (0, -1),           (0, 1),
        (1, -1),  (1, 0),  (1, 1)
    ]

    for dir_i, dir_j in directions:
        if (within_bounds(arr, i + dir_i, j+dir_j)):
            char = arr[i + dir_i][j+dir_j]
            if func(char):
                return True
    return False


def part1(problem):
    part_numbers = []

    for i, line in enumerate(problem):
        currentDigitsCoords = []
        currentDigits = []
        for j, character in enumerate(line):
            if hasDigit := isDigit(character):
                currentDigits.append(character)
                currentDigitsCoords.append((i, j))
            if not hasDigit or j == len(line) - 1:
                if currentDigits:
                    for i, j in currentDigitsCoords:
                        if checkAdjacent(problem, i, j, lambda char: char != "." and not isDigit(char)):
                            part_numbers.append(int("".join(currentDigits)))
                            break
                currentDigits = []
                currentDigitsCoords = []

    print(sum((part_numbers)))


def part2(problem):
    total = 0
    for i, line in enumerate(problem):
        for j, character in enumerate(line):
            if character == "*":
                adj = findAdjacents(problem, i, j, lambda char: isDigit(char))
                possible_gears = {}
                for (y, x), _ in adj:
                    curr_x = x
                    while (True):
                        curr_x -= 1
                        if not within_bounds(problem, y, curr_x) or not isDigit(problem[y][curr_x]):
                            curr_x += 1
                            break
                    x = curr_x
                    possible_gears[f"{y},{x}"] = problem[y][curr_x]
                    while (True):
                        curr_x += 1
                        if within_bounds(problem, y, curr_x) and isDigit(problem[y][curr_x]):
                            possible_gears[f"{y},{x}"] += problem[y][curr_x]
                        else:
                            break
                possible_gears = [int(v) for v in possible_gears.values()]
                if len(possible_gears) == 2:
                    total += possible_gears[0]*possible_gears[1]
    
    print(total)


problem = load("day3/input.txt")

part1(problem)

part2(problem)
