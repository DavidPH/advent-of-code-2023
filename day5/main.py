from typing import List, Tuple


def load(file: str):
    with open(file, "r") as f:
        return f.read().split("\n\n")


class Rule:
    def __init__(self, raw_rules: str):
        self.text = raw_rules
        self.mappings = []
        raw_rules = raw_rules.splitlines()[1:]
        for rule in raw_rules:
            self.mappings.append(list(map(int, rule.split())))

    def apply(self, seed: int) -> int:
        for dest, source, size in self.mappings:
            if seed < source or seed > source + size - 1:
                continue
            return seed + (dest - source)
        return seed


class Range:
    def __init__(self, start: int, end: int):
        self.start = start
        self.end = end

    def intersect(self, other: 'Range'):
        start = max(self.start, other.start)
        end = min(self.end, other.end)
        if start < end:
            return Range(start, end)
        return None

    def subtract(self, other: 'Range') -> List['Range']:
        parts = []
        if other.start > self.start:
            parts.append(Range(self.start, end=other.start))
        if other.end < self.end:
            parts.append(Range(other.end, self.end))
        return parts


def part1(problem):
    seeds, *stages = problem
    seeds = list(map(int, seeds.split()[1:]))
    for stage in stages:
        rule = Rule(stage)
        seeds = list(map(rule.apply, seeds))
    print(min(seeds))


def part2(problem):
    sr, *stages = problem
    sr = list(map(int, sr.split()[1:]))
    seed_ranges = [Range(sr[i], sr[i] + sr[i+1]) for i in range(0, len(sr), 2)]

    for stage in stages:
        stage = Rule(stage)

        new_seed_ranges = []
        while len(seed_ranges) > 0:
            seed_range = seed_ranges.pop()
            for dest, source, size in stage.mappings:
                overlap = seed_range.intersect(Range(source, source+size))

                if overlap:
                    new_seed_ranges.append(
                        Range(overlap.start - source + dest, overlap.end - source + dest))
                    seed_ranges += seed_range.subtract(overlap)
                    break
            else:
                new_seed_ranges.append(Range(seed_range.start, seed_range.end))
        seed_ranges = new_seed_ranges

    print(min(seed_ranges, key=lambda sr: sr.start).start)


problem = load("day5/input.txt")
part1(problem)
part2(problem)
