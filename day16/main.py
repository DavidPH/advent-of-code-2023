def load(file: str):
    with open(file, "r") as f:
        return [line.strip() for line in f]


P = complex


class Beam:
    def __init__(self, pos: 'P', dir: 'P'):
        self.pos = pos
        self.dir = dir

    def __eq__(self, other: 'Beam') -> bool:
        return self.pos == other.pos and self.dir == other.dir


class Tile:
    def __init__(self, char: str, loc: 'P'):
        self.char = char
        self.isEnergized = False
        self.loc = loc

    def passBeam(self, dir: 'P'):
        self.isEnergized = True

        if self.char == "|":
            # From left, or right
            if dir == P(0, 1) or dir == P(0, -1):
                return [P(1, 0), P(-1, 0)]  # upward and down
            else:
                return [dir]
        elif self.char == "-":
            # From up, or down
            if dir == P(-1, 0) or dir == P(1, 0):
                return [P(0, 1), P(0, -1)]  # left and right
            else:
                return [dir]
        elif self.char == "/":
            # From left
            if dir == P(0, 1):
                return [P(-1, 0)]  # up
            # From right
            elif dir == P(0, -1):
                return [P(1, 0)]  # down
            # From up
            elif dir == P(-1, 0):
                return [P(0, 1)]  # right
            # From down
            elif dir == P(1, 0):
                return [P(0, -1)]  # left
        elif self.char == "\\":
            # From left
            if dir == P(0, 1):
                return [P(1, 0)]  # up
            # From right
            elif dir == P(0, -1):
                return [P(-1, 0)]  # down
            # From up
            elif dir == P(-1, 0):
                return [P(0, -1)]  # left
            # From down
            elif dir == P(1, 0):
                return [P(0, 1)]  # right

        return [dir]


def traverseBeam(tiles: dict['P', 'Tile'], start: 'P', dir='P'):
    for tile in tiles.values():
        tile.isEnergized = False

    beams = [Beam(start, dir)]
    seen = {}
    count = 0
    while beams:
        current_beam = beams.pop()
        current_tile = tiles[current_beam.pos]
        if not current_tile.isEnergized:
            count += 1
        new_dirs = current_tile.passBeam(current_beam.dir)
        for dir in new_dirs:
            possible_loc = dir + current_tile.loc
            if possible_loc in tiles:
                beam = Beam(possible_loc, dir)
                if not (possible_loc, dir) in seen:
                    beams.append(beam)
                    seen[(possible_loc, dir)] = beam
    return count


def part1(problem):
    tiles: dict['P', 'Tile'] = {}

    for i, line in enumerate(problem):
        for j, char in enumerate(line):
            point = P(i, j)
            tiles[point] = Tile(char, point)

    count = traverseBeam(tiles, P(0, 0), P(0, 1))
    print(count)


def part2(problem):
    tiles: dict['P', 'Tile'] = {}

    for i, line in enumerate(problem):
        for j, char in enumerate(line):
            point = P(i, j)
            tiles[point] = Tile(char, point)

    height = len(problem)
    width = len(problem)

    max_count = 0
    for i in range(height):
        starts = [P(i, 0), P(i, width - 1)]
        dirs = [P(0, 1), P(0, -1)]
        for s, d in zip(starts, dirs):
            count = traverseBeam(tiles, s, d)
            if count > max_count:
                max_count = count

    for i in range(width):
        starts = [P(0, i), P(height - 1, i)]
        dirs = [P(1, 0), P(-1, 0)]
        for s, d in zip(starts, dirs):
            count = traverseBeam(tiles, s, d)
            if count > max_count:
                max_count = count

    print(max_count)


problem = load("day16/input.txt")
part1(problem)
part2(problem)
