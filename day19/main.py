import re


def load(file: str):
    with open(file, "r") as f:
        return list(map(lambda l: l.splitlines(), f.read().split("\n\n")))


class Part:
    def __init__(self, part_str: str) -> None:
        self.str = part_str
        self.x, self.m, self.a, self.s = self.deserialize()

    def deserialize(self):
        return tuple(map(int, re.findall("\d+", self.str)))

    def sum(self):
        return self.x + self.m + self.a + self.s


class Workflow:
    def __init__(self, workflow_str: str) -> None:
        self.str = workflow_str
        self.name, rest = workflow_str.split("{")
        self.rules = rest.split("}")[0].split(",")

    def parsedRules(self):
        rules = []
        fallback = self.rules[-1]
        for rule in self.rules[:-1]:
            key = rule[0]
            cmp = rule[1]
            rule, target = rule.split(":")
            rule = rule[2:]
            rules.append((key, cmp, int(rule), target))
        return rules, fallback

    def parsePart(self, part: 'Part'):
        for rule in self.rules[:-1]:
            rule, go_to = rule.split(":")
            if eval(rule,  {"x": part.x, "m": part.m, "a": part.a, "s": part.s}):
                return go_to
        return self.rules[-1]


def part1(problem):
    raw_workflows, raw_parts = problem
    parts: list['Part'] = []
    accepted: list['Part'] = []
    for part in raw_parts:
        parts.append(Part(part))

    workflows: dict[str, 'Workflow'] = {}
    for workflow in raw_workflows:
        w = Workflow(workflow)
        workflows[w.name] = w

    while parts:
        part = parts.pop()
        workflow = workflows["in"]
        name = workflow.name
        while name != "A" and name != "R":
            workflow = workflows[name]
            name = workflow.parsePart(part)
        if name == "A":
            accepted.append(part)

    print(sum([x.sum() for x in accepted]))


def part2(problem):
    raw_workflows, _ = problem

    workflows: dict[str, 'Workflow'] = {}

    for w in raw_workflows:
        workflow = Workflow(w)
        workflows[workflow.name] = workflow

    def count(ranges: dict[str, tuple[int, int]], name="in"):
        if name == "R":
            return 0
        if name == "A":
            product = 1
            for lo, hi in ranges.values():
                product *= hi - lo + 1
            return product

        rules, fallback = workflows[name].parsedRules()

        total = 0

        for key, cmp, n, target in rules:
            lo, hi = ranges[key]
            if cmp == "<":
                T = (lo, min(n - 1, hi))
                F = (max(n, lo), hi)
            else:
                T = (max(n + 1, lo), hi)
                F = (lo, min(n, hi))
            if T[0] <= T[1]:
                copy = dict(ranges)
                copy[key] = T
                total += count(copy, target)
            if F[0] <= F[1]:
                ranges = dict(ranges)
                ranges[key] = F
            else:
                break
        else:
            total += count(ranges, fallback)

        return total

    print(count({key: (1, 4000) for key in "xmas"}))


problem = load("day19/input.txt")
part1(problem)
part2(problem)
