from collections import Counter


def load(file: str):
    with open(file, "r") as f:
        return [line.strip().split() for line in f]


class Hand:
    types = [
        (1, 1),  # High card
        (2, 1),  # One pair
        (2, 2),  # Two pair
        (3, 1),  # Three of a kind
        (3, 2),  # Full house
        (4, 1),  # Four of a kind
        (5, 0),  # Five of a kind
    ]

    def __init__(self, cards: str, bid: int, useJoker=False):
        self.cards = cards
        self.bid = bid
        self.useJoker = useJoker
        r = {'T': 10, 'J': 11*(not useJoker), 'Q': 12, 'K': 13, 'A': 14}
        self.cards = tuple(r[c] if c in r else int(c) for c in self.cards)
        self.type = self.get_type()

    def get_type(self):
        counter = Counter(self.cards)
        J_count = counter[0]
        if self.useJoker and 0 in counter:
            counter[0] = 0
        most_common = counter.most_common(2)
        i = most_common[0][1] + J_count * self.useJoker
        j = most_common[1][1] if len(most_common) > 1 else 0
        return Hand.types.index((i, j))

    def __lt__(self, other: 'Hand') -> bool:
        return self.type < other.type or (self.type == other.type and self.cards < other.cards)


def calc_score(hands: list['Hand']) -> int:
    total = 0
    for i, hand in enumerate(sorted(hands)):
        total += (i+1)*hand.bid
    return total


def part1(problem):
    hands = []
    for cards, bid in problem:
        hand = Hand(cards, int(bid))
        hands.append(hand)
    score = calc_score(hands)
    print(score)


def part2(problem):
    hands = []
    for cards, bid in problem:
        hand = Hand(cards, int(bid), True)
        hands.append(hand)
    score = calc_score(hands)
    print(score)


problem = load("day7/input.txt")

part1(problem)
part2(problem)
