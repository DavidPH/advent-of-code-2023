
def load(file: str):
    with open(file, "r") as f:
        return [line.strip() for line in f]


P = complex


class Pipe:
    types = {
        "|": (P(-1, 0), P(1, 0)),  # North and South
        "-": (P(0, -1), P(0, 1)),  # West and East
        "L": (P(-1, 0), P(0, 1)),  # North and East
        "J": (P(-1, 0), P(0, -1)),  # North and West
        "7": (P(1, 0), P(0, -1)),  # South and West
        "F": (P(1, 0), P(0, 1))  # South and East
    }

    def __init__(self, type: str, loc: P):

        self.type = type
        self.loc = loc
        self.connections = Pipe.types.get(type, None)

    def getConnections(self, pipes: dict[P, 'Pipe']) -> list['Pipe']:
        connections: list['Pipe'] = []
        if self.type == "S":
            directions = [
                P(-1, -1), P(-1, 0), P(-1, 1),
                P(0, -1),           P(0, 1),
                P(1, -1),  P(1, 0),  P(1, 1)
            ]
            for dir in directions:
                new_dir = self.loc + dir
                if new_dir in pipes and self.isConnected(pipes[new_dir]):
                    connections.append(pipes[new_dir])
        else:
            for connection in self.connections:
                dir = connection + self.loc
                if dir in pipes:
                    connections.append(pipes[dir])
        return connections

    def isConnected(self, other: 'Pipe'):
        if self.type == "S":
            other, self = self, other
        for connection in self.connections:
            if other.loc - connection == self.loc:
                return True
        return False

    def __eq__(self, other: 'Pipe') -> bool:
        if type(other) == complex:
            return self.loc == other
        return self.loc == other.loc and self.type == other.type

    def __repr__(self) -> str:
        return f"{self.loc} {self.type}"


problem = load(file="day10/input.txt")
start: 'Pipe' = None
network: dict[P, 'Pipe'] = {}
grid = {}

for i, line in enumerate(problem):
    for j, char in enumerate(line):
        point = P(i, j)
        if char != ".":
            network[point] = Pipe(char, point)
            if char == "S":
                start = network[point]
        grid[point] = char


# Tried and failed :( squeezing through pipes requires extending the maze by 1?
def floodFill(color: int, point: P, filled: dict[P, int]):
    directions = [
        P(-1, -1), P(-1, 0), P(-1, 1),
        P(0, -1),           P(0, 1),
        P(1, -1),  P(1, 0),  P(1, 1)
    ]
    if point in filled or not point in grid:
        return color
    if not point in loop:
        filled[point] = color
    for dir in directions:
        new_dir = point + dir
        if new_dir in network:
            continue
        floodFill(color, new_dir, filled)
    return color + 1


def part1():
    loop: dict[P, 'Pipe'] = {}
    prev_pipe = start
    current = start.getConnections(network)[0]
    while current != start:
        loop[current.loc] = current
        adj = current.getConnections(network)
        for new_pipe in adj:
            if new_pipe != prev_pipe:
                break
        prev_pipe = current
        current = new_pipe
    print((len(loop) + 1) // 2)
    return loop


def part2(loop: dict[P, 'Pipe']):
    count = 0
    loop[start.loc] = Pipe("F", start.loc)
    for i, line in enumerate(problem):
        inside = False
        for j, char in enumerate(line):
            point = P(i, j)
            if point not in loop:
                if inside:
                    count += 1
            else:
                if char in ["L", "|", "J"]:
                    inside = not inside
    print(count)


loop = part1()
part2(loop)
