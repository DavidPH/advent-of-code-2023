from dataclasses import dataclass
from sympy import symbols, Symbol, solve_poly_system


def load(file: str):
    with open(file, "r") as f:
        return [list(map(lambda l: list(map(int, l.split(","))), line.strip().split("@"))) for line in f]


@dataclass
class Point:
    x: float
    y: float

    vx: float
    vy: float


def find_intersection(p1: 'Point', p2: 'Point', p3: 'Point', p4: 'Point'):
    m1 = (p2.y - p1.y) / (p2.x - p1.x)
    m2 = (p4.y - p3.y) / (p4.x - p3.x)

    b1 = p1.y - m1 * p1.x
    b2 = p3.y - m2 * p3.x

    if m1 == m2:
        return None

    x_intersection = (b2 - b1) / (m1 - m2)
    y_intersection = m1 * x_intersection + b1

    return Point(x_intersection, y_intersection, 0, 0)


def part1(problem):
    lower = 200000000000000
    upper = 400000000000000
    count = 0
    for i, ((x1, y1, _), (vx, vy, _)) in enumerate(problem[:-1]):
        A = Point(x1, y1, vx, vy)
        B = Point(A.x + A.vx, A.y + A.vy, vx, vy)

        for (x1, y1, _), (vx, vy, _) in problem[i:]:
            C = Point(x1, y1, vx, vy)
            D = Point(C.x + C.vx, C.y + C.vy, vx, vy)

            intersection = find_intersection(A, B, C, D)
            if intersection is None:
                continue
            if lower <= intersection.x <= upper and lower <= intersection.y <= upper:
                if (all(((intersection.x - P.x) * P.vx >= 0) and ((intersection.y - P.y) * P.vy >= 0) for P in (A, C))):
                    count += 1

    print(count)


def part2(problem):
    equations = []
    x, y, z, vx, vy, vz = symbols("x, y, z, vx, vy, vz")
    p = [x, y, z]
    v = [vx, vy, vz]
    vars = [*p, *v]
    for i, (points, velocities) in enumerate(problem[:3]):
        t = Symbol(f"t_{i}")
        for j in range(3):
            equations.append(p[j] + v[j] * t - points[j] + velocities[j]*t)
        vars.append(t)
    print(sum(solve_poly_system(equations, vars)[0][:3]))


problem = load("day24/input.txt")
part1(problem)
part2(problem)
